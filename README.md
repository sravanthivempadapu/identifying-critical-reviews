# Identifying-critical-reviews

This projects looks at various submitted by users for different apps and identifies the reviews that has positive content in review but given less rating.

## Running in local environment
If one wants to try thin in local environment, please download the project folder or clone it in local- 

Step1 - Please create a virtual environment and install the packages in the requirement.txt

Step2 - Go inside the folder (identifying-critical-reviews/)

Step3 - run the app script as - python app.py

Step4 - open a browser and copy the link - http://127.0.0.1:5000/

Step5 - Try logging with following credentials -
username as - Sravanthi
password as - Password

Step6 - Upload either the sample reviews file in data folder or the reviews file of your interest (Note - Column names and format should match with the sample files given in data folder)

Step7 - Once uploaded click on the submit button

Step8 - Upon success the reviews of interest are saved in data folder
