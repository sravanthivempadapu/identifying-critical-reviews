from flask import Flask, render_template, request, abort, redirect, url_for
import pickle
from http import HTTPStatus as hs
from aiohttp import web
import pandas as pd
import flair
import pickle

# model = pickle.load(open("flair_sa.pkl", 'rb'))

app = Flask(__name__)

@app.route('/')
def index():
    return redirect(url_for('login'))

@app.route("/login", methods = ["POST", "GET"])
def login():
    if request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]
        if username == "Sravanthi" and password == "Password":
            return redirect('uploads')
        else:
            return render_template('login.html')
    else:
        return render_template('login.html')

@app.route("/uploads", methods = ["GET","POST"])
def uploads():
    if request.method == "POST":
        try:
            uploaded_file = request.files['file']
            reviews = pd.read_csv(uploaded_file)
            indetify_critical_reviews(reviews)
            return f"<h1> Successful, identify the critical reviews in data folder in file Reviews of interest.csv</h1>"
        except:
            return f"<h1> Unable to process the given data, please verify the format</h1>"
    else:
        return render_template('uploads.html')

def indetify_critical_reviews(reviews):
    reviews_text = reviews[["Text"]]
    reviews_rating = reviews[["Star"]]
    with open('./models/flair_sa.pkl', 'rb') as file:
       sentence_model = pickle.load(file)
    reviews_text['Rating'] = reviews_text['Text'].apply(lambda x: predict_rating(sentence_model, x))
    final_review = pd.concat([reviews_text, reviews_rating],axis=1)
    reviews_of_interest = final_review[final_review['Rating']-final_review['Star']>2]
    reviews_of_interest.to_csv("./data/Reviews of interest.csv")


def predict_rating(sentence_model, text):
    print(text)
    sentence=flair.data.Sentence(text)
    sentence_model.predict(sentence)
    total_sentiment = sentence.labels
    
    if "NEGATIVE" in str(total_sentiment[0]):
        if sentence.score > 0.75:
            return 1
        else:
            return 2
    else:
        if sentence.score > 0.85:
            return 5
        elif sentence.score > 0.75:
            return 4
        else:
            return 3



if __name__ == "__main__":
    app.run(debug=True)